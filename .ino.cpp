#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2017-05-30 19:17:05

#include "Arduino.h"
#include "Arduino.h"
#include <vector>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266HTTPClient.h>
#include "LEDStripCtrl.h"
void setup() ;
void loop() ;
void doScan() ;
void doProbe() ;
void doRangeTest() ;
bool isOnBlacklist(String bssid) ;
float clamp(float x, float min, float max) ;


#include "WifiAnalyzer.ino"

#endif
